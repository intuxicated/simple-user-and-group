<?php


namespace App\JsonTransformer;


use App\Exceptions\ApiProblem;
use App\Exceptions\ApiProblemException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Translation\TranslatorInterface;

class JsonTransformer
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * JsonTransformer constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }


    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $content = $request->getContent();
        if (empty($content)) {
            return;
        }
        if (! $this->isJsonRequest($request)) {
            return;
        }
        if (! $this->transformJsonBody($request)) {
            throw new ApiProblemException(new ApiProblem(400,$this->translator->trans("invalid json body",[],"validators")));
        }
    }
    private function isJsonRequest(Request $request)
    {
        return 'json' === $request->getContentType();
    }
    private function transformJsonBody(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            return false;
        }
        if ($data === null) {
            return true;
        }
        $request->request->replace($data);
        return true;
    }

}
