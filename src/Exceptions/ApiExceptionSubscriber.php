<?php


namespace App\Exceptions;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ApiExceptionSubscriber
{
    private $debug;
    /**
     * @var Request $request
     */
    private $request;

    /**
     * ApiExceptionSubscriber constructor.
     * @param $debug
     * @param RequestStack $requestStack
     */
    public function __construct($debug, RequestStack $requestStack) {
        $this->debug = $debug;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function onKernelException(GetResponseForExceptionEvent $event) {
        $e = $event->getException();

        $statusCode = $e instanceof HttpExceptionInterface ? $e->getStatusCode() : 500;

        if (! $this->request->headers->get('content-type') === 'application/json') {
            return;
        }
        // allow 500 errors to be thrown
        if ($this->debug && $statusCode >= 500) {
            return $event->getResponse();
        }

        if ($e instanceof ApiProblemException) {
            $apiProblem = $e->getApiProblem();
        } else {
            $apiProblem = new ApiProblem(
              $statusCode
            );

            if ($e instanceof HttpExceptionInterface) {
                $apiProblem->set('detail', $e->getMessage());
            }
        }

        $data = $apiProblem->toArray();


        $response = new JsonResponse(
          $data,
          $apiProblem->getStatusCode()
        );
        $response->headers->set('Content-Type', 'application/problem+json');
        $event->setResponse($response);
        return $response;
    }


}
