<?php


namespace App\Controller\Api\Admin;


use App\Controller\Api\BaseController;
use App\Entity\Group;
use App\Entity\User;
use App\Form\GroupType;
use App\Form\UserType;
use App\Repository\GroupRepository;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("user")
 */
class UserController extends BaseController
{

    /**
     * @Route("/", methods={"GET"})
     * @param UserRepository $userRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(UserRepository $userRepository)
    {
        return $this->createApiResponse($userRepository->findAll(), 200, ['user-overview', 'user-groups', 'group-overview']);
    }

    /**
     * @Route("/{id}", methods={"GET"})
     *
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(User $user)
    {
        return $this->createApiResponse($user, 200, ['user-overview', 'user-groups', 'group-overview']);
    }

    /**
     * @Route("/", methods={"POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user, ['csrf_protection' => false]);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            return $this->createApiResponse($user, 200, ['user-overview', 'user-groups', 'group-overview']);
        }

        return $this->throwApiProblemValidationException($form);
    }

    /**
     * @Route("/{id}", methods={"PUT"})
     *
     * @param User $user
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(User $user, Request $request)
    {
        $form = $this->createForm(UserType::class, $user, ['csrf_protection' => false]);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->createApiResponse($user, 200, ['user-overview', 'user-groups', 'group-overview']);
        }

        return $this->throwApiProblemValidationException($form);
    }

    /**
     * @Route("/{id}", methods={"DELETE"})
     *
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function delete(User $user)
    {
        $this->getDoctrine()->getManager()->remove($user);
        $this->getDoctrine()->getManager()->flush();

        return $this->createApiResponse(null, 204);
    }
}
