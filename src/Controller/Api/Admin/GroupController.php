<?php


namespace App\Controller\Api\Admin;


use App\Controller\Api\BaseController;
use App\Entity\Group;
use App\Form\GroupType;
use App\Repository\GroupRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("group")
 */
class GroupController extends BaseController
{

    /**
     * @Route("/", methods={"GET"})
     * @param GroupRepository $groupRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(GroupRepository $groupRepository)
    {
        return $this->createApiResponse($groupRepository->findAll(), 200, ['group-overview']);
    }

    /**
     * @Route("/{id}", methods={"GET"})
     *
     * @param Group $group
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(Group $group)
    {
        return $this->createApiResponse($group, 200, ['group-overview']);
    }

    /**
     * @Route("/", methods={"POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $group = new Group();
        $form = $this->createForm(GroupType::class, $group, ['csrf_protection' => false]);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->persist($group);
            $this->getDoctrine()->getManager()->flush();

            return $this->createApiResponse($group, 200, ['group-overview']);
        }

        return $this->throwApiProblemValidationException($form);
    }

    /**
     * @Route("/{id}", methods={"PUT"})
     *
     * @param Group $group
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(Group $group, Request $request)
    {
        $form = $this->createForm(GroupType::class, $group, ['csrf_protection' => false]);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->createApiResponse($group, 200, ['group-overview']);
        }

        return $this->throwApiProblemValidationException($form);
    }

    /**
     * @Route("/{id}", methods={"DELETE"})
     *
     * @param Group $group
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function delete(Group $group, Request $request)
    {
        $form = $this->createFormBuilder($group, ['csrf_protection' => false, 'validation_groups' => ['removing'], 'data_class' => Group::class])->getForm();
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->remove($group);
            $this->getDoctrine()->getManager()->flush();

            return $this->createApiResponse(null, 204);
        }

        return $this->throwApiProblemValidationException($form);
    }
}
