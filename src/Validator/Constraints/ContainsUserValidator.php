<?php


namespace App\Validator\Constraints;


use App\Entity\Group;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ContainsUserValidator extends ConstraintValidator
{
    /**
     * @param Group $protocol
     * @param ContainsUser $constraint
     */
    public function validate($protocol, Constraint $constraint)
    {
        if ($protocol->getUsers()->count() > 0) {
            $this->context->buildViolation($constraint->message)
                ->atPath('users')
                ->addViolation();
        }
    }

}
