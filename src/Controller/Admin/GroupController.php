<?php

namespace App\Controller\Admin;

use App\Entity\Group;
use App\Form\GroupType;
use App\Repository\GroupRepository;
use FOS\RestBundle\Context\Context;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/group")
 */
class GroupController extends Controller
{
    /**
     * @Route("/", name="admin_group_index", methods="GET")
     * @param GroupRepository $groupRepository
     * @param Request $request
     * @return Response
     */
    public function index(GroupRepository $groupRepository, Request $request): Response
    {
        return $this->render('admin/group/index.html.twig', ['groups' => $groupRepository->findAll()]);
    }

    /**
     * @Route("/new", name="admin_group_new", methods="GET|POST")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $group = new Group();
        $form = $this->createForm(GroupType::class, $group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();

            return $this->redirectToRoute('admin_group_index');
        }

        return $this->render('admin/group/new.html.twig', [
            'group' => $group,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_group_show", methods="GET")
     * @param Group $group
     * @return Response
     */
    public function show(Group $group): Response
    {
        return $this->render('admin/group/show.html.twig', ['group' => $group]);
    }

    /**
     * @Route("/{id}/edit", name="admin_group_edit", methods="GET|POST")
     * @param Request $request
     * @param Group $group
     * @return Response
     */
    public function edit(Request $request, Group $group): Response
    {
        $form = $this->createForm(GroupType::class, $group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_group_edit', ['id' => $group->getId()]);
        }

        return $this->render('admin/group/edit.html.twig', [
            'group' => $group,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_group_delete", methods="DELETE")
     * @param Request $request
     * @param Group $group
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function delete(Request $request, Group $group, ValidatorInterface $validator): Response
    {
        if ($this->isCsrfTokenValid('delete'.$group->getId(), $request->request->get('_token'))) {
            $errors = $validator->validate($group, null, [
                'removing'
            ]);

            if (count($errors) > 0) {
                $errorsString = '';
                foreach ($errors as $err) {
                    $errorsString .= $err->getMessage().'<br>';
                }

                return new Response($errorsString);
            }

            $em = $this->getDoctrine()->getManager();
            $em->remove($group);
            $em->flush();
        }

        return $this->redirectToRoute('admin_group_index');
    }
}
