<?php

namespace App\Controller\Api;

use App\Exceptions\ApiProblem;
use App\Exceptions\ApiProblemException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;

abstract class BaseController extends Controller
{
    /**
     * @param $data
     * @param array $groups
     *
     * @return mixed|string
     */
    protected function serializeToJson($data, $groups = ['Default'])
    {
        $json = $this->container->get('serializer')
            ->serialize($data, 'json',
                ['groups' => $groups]);

        return $json;
    }

    /**
     * @param $data
     * @param int   $statusCode
     * @param array $groups
     *
     * @return Response
     */
    protected function createApiResponse(
        $data,
        $statusCode = 200,
        $groups = ['Default']
    ) {
        $response = new Response(
            $this->serializeToJson($data, $groups), $statusCode,
            ['Content-Type' => 'application/json']
        );

        return $response;
    }

    /**
     * @param FormInterface $form
     */
    public function throwApiProblemValidationException(FormInterface $form)
    {
        $errors = $this->getErrorsFromForm($form);
        $apiProblem = new ApiProblem(
            400,
            'validation_error'
        );
        $apiProblem->set('errors', $errors);
        throw new ApiProblemException($apiProblem);
    }

    /**
     * @param FormInterface $form
     *
     * @return array
     */
    protected function getErrorsFromForm(FormInterface $form)
    {
        $errors = [];

        foreach ($form->getErrors() as $error) {

            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $key => $child) {
            /** @var $child FormInterface */
            if ($err = $this->getErrorsFromForm($child)) {
                $errors[$key] = $err;
            }
        }
        return $errors;
    }

    /**
     * @param string $title
     * @param int    $statusCode
     *
     * @return ApiProblemException
     */
    protected function createApiProblemException($title, $statusCode = 404)
    {
        return new ApiProblemException(new ApiProblem($statusCode, $title));
    }
}
